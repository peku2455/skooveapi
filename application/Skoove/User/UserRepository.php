<?php

namespace Skoove\User;


use Doctrine\ORM\EntityManagerInterface;
use Skoove\Contracts\Repository;

class UserRepository implements Repository
{
    protected $persistence;
    const  entity=User::class;

    /**
     * UserRepository constructor.
     */
    public function __construct(EntityManagerInterface $persistence)
    {
        $this->persistence = $persistence;
    }

    function findById(int $id)
    {
        return $this->persistence->find(self::entity, $id);
    }

    function save($data, User $user=null)
    {
        if (!$user) {
            $user = $this->getEntity();
        }

        try{
            $this->persistence->beginTransaction();
            $this->mapDataToUser($data, $user);
            $this->persistence->persist($user);
            $this->persistence->flush();
            $this->persistence->commit();
        }catch (\Exception $exception){
            $this->persistence->rollback();
            throw $exception;
        }

        return $user;
    }

    /**
     * @param $data
     * @param $user
     */
    public function mapDataToUser($data, $user)
    {
        if ($data->email)
            $user->setEmail($data->email);
        if ($data->name)
            $user->setName($data->name);
        if ($data->password)
            $user->setPassword(sha1($data->password));
        return $user;
    }


    /**
     * @return User
     */
    public function getEntity(): User
    {
        $class = self::entity;
        $user = new $class;
        return $user;
    }

}