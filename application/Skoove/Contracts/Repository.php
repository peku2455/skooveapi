<?php
namespace Skoove\Contracts;


interface Repository{
  function findById(int $id);
  function save($data);
}