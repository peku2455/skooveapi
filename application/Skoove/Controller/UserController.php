<?php


namespace Skoove\Controller;


use Skoove\Contracts\Repository;
use Skoove\User\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController
{
    /** @var UserRepository */
    protected $repo;

    public function __construct(Repository $repo, Response $response)
    {
        $this->repo = $repo;
        $this->response = $response;
    }

    public function indexJsonAction(int $id)
    {
        $message = false;
        $status = 200;
        try {
            $user = $this->repo->findById($id);
        } catch (\Exception $e) {
            $message = true;
            $status = 404;
        }
        return $this->mapResponse($status, $user, $message);
    }

    public function postJsonAction(Request $request)
    {
        $status = 403;
        $message = false;
        try {
            if ($this->isValidRequest($request)) {
                $data = $this->repo->save(json_decode($request->getContent()));
                $status = 200;
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            $status = 409;
        }
        return $this->mapResponse($status, $data, $message);
    }

    public function putJsonAction($id, Request $request)
    {
        $status = 404;
        $message = true;
        try {
            if ($this->isValidRequest($request)) {
                $user = $this->repo->findById($id);
                if ($user) {
                    $data = $this->repo->save(json_decode($request->getContent()), $user);
                    $status = 200;
                }
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            $message = false;
            $status = 409;
        }
        return $this->mapResponse($status, $data, $message);
    }

    /**
     * Create response for all actions
     * @param $successful
     * @param null $data
     * @param bool $withMessage
     * @return Response
     */
    public function mapResponse($status = 200, $data = null, $withMessage = false): Response
    {
        $context['data'] = (array)$data;
        $context['data']['type'] = 'user';
        if (in_array($status, [200, 201])) {
            $withMessage ? $context['message'] = 'Action successful' : null;
        } else {
            $withMessage ? $context['message'] = 'Action failed' : null;
        }

        $response = $this->response;
        $response = new $response($context, $status);

        return $response;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isValidRequest(Request $request): bool
    {
        return 0 === strpos($request->headers->get('Content-Type'), 'application/json');
    }
}