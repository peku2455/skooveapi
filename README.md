# README #

### What is this repository for? ###

* Backend api for skoove
* Version 0.0.1
* [Web app in progress](https://bitbucket.org/peku2455/skooveweb)

### How do I get set up? ###

* To set up you can use the docker from [Web app](https://bitbucket.org/peku2455/skooveweb)
* Database can e created with `vendor/bin/doctrine orm:schema-tool:create`

### Other info ###

* The code do not contain tests