<?php
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

require_once __DIR__ . '/vendor/autoload.php';

$isDevMode = true;
$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/application/Skoove/config/yaml"), $isDevMode);

$conn = array(
        'dbname' => 'skoove',
        'user' => 'skoove',
        'password' => 'skoove',
        'host' => 'mysql',
        'driver' => 'pdo_mysql',
);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);