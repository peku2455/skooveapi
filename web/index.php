<?php


use Skoove\Controller\UserController;
use Skoove\User\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

require_once __DIR__ . '/../bootstrap.php';

$app = new Silex\Application();
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app['response.repository'] = function (){
    return new JsonResponse();
};

$app['users.repository'] = function () use ($entityManager) {
    return new UserRepository($entityManager);
};

$app['users.controller'] = function () use ($app) {
    return new UserController($app['users.repository'], $app['response.repository']);
};

$app->get('/user/{id}', "users.controller:indexJsonAction")->assert('id', '\d+');
$app->post('/user', "users.controller:postJsonAction");
$app->put('/user/{id}', "users.controller:putJsonAction")->assert('id', '\d+');


$app->run();
